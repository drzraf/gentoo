# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sci-geosciences/zvtm-svg/zvtm-svg-9999.ebuild,v 1.1 2011/06/17 10:00:39 scarabeus Exp $

EAPI=4
##inherit eutils  subversion java-mvn-src
#inherit eutils java-maven-2 subversion

JAVA_PKG_IUSE="source doc"
JAVA_MAVEN_BOOTSTRAP="Y"

DESCRIPTION="ZGRViewer is a graph visualizer implemented in Java and based upon the Zoomable Visual Transformation Machine"
HOMEPAGE="http://zvtm.sourceforge.net/zgrviewer.html"
ESVN_REPO_URI="https://zvtm.svn.sourceforge.net/svnroot/zvtm/zvtm-svg/trunk"

LICENSE="LGPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=">=virtual/jdk-1.6"
RDEPEND=">=virtual/jre-1.6"

#S="${WORKDIR}/${PN}"

IUSE=""
