# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sci-geosciences/zgrviewer/zgrviewer-9999.ebuild,v 1.1 2011/06/17 10:00:39 scarabeus Exp $

EAPI=4
inherit eutils  subversion
#inherit eutils java-mvn-src subversion

DESCRIPTION="ZGRViewer is a graph visualizer implemented in Java and based upon the Zoomable Visual Transformation Machine"
HOMEPAGE="http://zvtm.sourceforge.net/zgrviewer.html"
ESVN_REPO_URI="https://zvtm.svn.sourceforge.net/svnroot/zvtm/zgrviewer/trunk"

LICENSE="LGPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-java/maven-bin
		app-admin/eselect-maven
		>=virtual/jdk-1.6"

RDEPEND=">=virtual/jre-1.6"

S="${WORKDIR}/${PN}"

IUSE=""

src_prepare() {
	# patch zvtm-svg 0.2.0
	echo "a"
}

src_compile() {
	mvn compile
}

src_install() {
	#java-pkg_newjar "dist/${PN}.jar" || die "java-pkg_newjar failed"
	#java-pkg_dolauncher "${PN}" --jar "${PN}.jar" || die "java-pkg_dolauncher failed"

	#newicon images/logo.png josm.png || die "newicon failed"
	#make_desktop_entry "${PN}" "Java OpenStreetMap Editor" josm "Science;Geoscience"
	echo "b"
}
