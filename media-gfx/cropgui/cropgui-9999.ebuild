# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

# TODO; icons into /usr/share
EAPI=4

EGIT_REPO_URI="git://git.unpythonic.net/cropgui.git"

inherit git-2 eutils

DESCRIPTION="GUI for lossless jpeg cropping"
HOMEPAGE="http://emergent.unpythonic.net/01248401946"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="dev-python/pygobject
		 gnome-base/libglade"
DEPEND="${RDEPEND}"

src_install() {
	./install.sh -t "${D}" -p /
}
