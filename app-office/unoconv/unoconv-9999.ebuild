# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

EGIT_REPO_URI="git://github.com/dagwieers/unoconv.git"

inherit git-2 eutils

DESCRIPTION="Convert between any document format supported by OpenOffice"
HOMEPAGE="http://dag.wieers.com/home-made/unoconv/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

DEPEND="app-text/asciidoc
	dev-python/setuptools"
RDEPEND="|| ( app-office/openoffice app-office/openoffice-bin
			  app-office/libreoffice app-office/libreoffice-bin )"

src_prepare() {
	epatch "${FILESDIR}/${P}-longer-timeout.patch"
}

src_install() {
	emake install DESTDIR="${D}" bindir='$(prefix)/share/unoconv' || die
	newbin "${FILESDIR}"/unoconv-wrapper unoconv || die
}
