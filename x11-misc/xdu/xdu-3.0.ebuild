# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="A Disk Usage (DU) or other tree data display program"
HOMEPAGE="http://sd.wareonearth.com/~phil/xdu/"
SRC_URI="${HOMEPAGE}/${P}.tar.Z"

LICENSE="xboing"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

RDEPEND="x11-libs/libXaw
		 x11-libs/libXt
		 x11-libs/libXext
		 x11-libs/libX11"
DEPEND="${RDEPEND}
		x11-misc/imake"

S=${WORKDIR}

src_compile() {
		xmkmf || die "xmkmf failed"
		emake
}

src_install() {
	dobin xdu || die "dobin failed"
	newman xdu.man xdu.1 || die "newman failed"
	dodoc README || die "dodoc failed"
}
