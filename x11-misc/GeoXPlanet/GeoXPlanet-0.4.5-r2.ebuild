# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
inherit eutils

DESCRIPTION="Wrapper script to draw traceroutes of current network connections on xplanet images"
HOMEPAGE="http://geoxplanet.sourceforge.net"
SRC_URI="mirror://sourceforge/geoxplanet/${PF}_.tar.bz2"
#mirror://sourceforge/${PN}/geoip.db.zip

LICENSE="GPL-1 geoip? ( MaxMind )"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="xinerama geoip"

RDEPEND="x11-misc/xplanet
	dev-python/pygtk
	xinerama? ( media-gfx/imagemagick )
	geoip? ( dev-libs/geoip )
	|| (
		( >=dev-python/pysqlite-2.0.3 <dev-lang/python-2.5 )
		>=dev-lang/python-2.5[sqlite]
	)"

src_unpack() {
	unpack ${A}
	cd "${S}"

	# Originally GeoXPlanet moves temp files (images, config files, etc...) into user's home at first launch
	# This patch changes move operation to copy
	epatch "${FILESDIR}/${PF}.patch"
}

src_install() {
	dobin "${FILESDIR}/geoxplanet"

	cd "${WORKDIR}"
	#insinto /usr/share/GeoXPlanet/
	dodoc CHANGELOG CONFIG_OPTIONS README
	#cd "${S}"
	doins -r arcFiles defaults src temp templates || die
	insinto /usr/share/GeoXPlanet/src
	insopts -m755
	doins src/GeoXPlanet.py src/configGUI.py || die

	ewarn "GeoXPlanet will show configuration dialog only at first launch, but you can type 'geoxplanet -C' to use it later."
}
