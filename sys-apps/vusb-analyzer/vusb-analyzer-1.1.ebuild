# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-apps/vusb-analyzer/vusb-analyzer-1.1.ebuild,v 1.3 2011/08/03 03:55:42 gibboris Exp $

inherit python

DESCRIPTION="Tool for visualizing logs of USB packets"
HOMEPAGE="http://vusb-analyzer.sourceforge.net"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~ppc ~amd64"
IUSE=""

PYTHON_DEPEND="2"
# TODO:
# DOCS="README.txt CHANGELOG.txt"

RDEPEND="dev-python/libgnomecanvas-python
		 dev-python/pygtk"
# optional:	 dev-python/psyco

src_install() {
	dobin vusb-analyzer
	dodoc README.txt CHANGELOG.txt
	mkdir -p ${D}/$(python_get_libdir)/site-packages
	cp -r VUsbTools ${D}/$(python_get_libdir)/site-packages/
}
