# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
#EAPI=4-python

PYTHON_MULTIPLE_ABIS=1
PYTHON_RESTRICTED_ABIS="3.*"
# PYTHON_DEPEND="2:2.6"
PYTHON_USE_WITH="sqlite"
DISTUTILS_SRC_TEST="setup.py"

inherit distutils python git-2

EGIT_REPO_URI="https://github.com/geier/khal.git"
EGIT_BRANCH="master"

DESCRIPTION="A CardDAV based address book tool"
HOMEPAGE="http://lostpackets.de/khal/"

LICENSE="MIT"
KEYWORDS="~x86 ~amd64"
SLOT="0"
IUSE=""

DEPEND="dev-python/lxml
	dev-python/icalendar
	dev-python/requests
	dev-python/urwid
	dev-python/pyxdg
	dev-python/python-dateutil
	dev-python/setuptools
	virtual/python-argparse"
#	dev-python/vobject

RDEPEND="${DEPEND}
		 dev-python/pytz"

DOCS="README.rst khal.conf.sample doc/parts/usage.rst doc/parts/faq.rst"

pkg_postinst() {
	ewarn "Copy and edit the supplied khal.conf.sample file"
	ewarn "(default location is ~/.khal/khal.conf)."
	ewarn "Beware that only you can access this file,"
	ewarn "if you have untrusted users on your machine,"
	ewarn "since the password is stored in cleartext."
}
