# Copyright 2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit autotools git-2

EGIT_REPO_URI="git://git.code.sf.net/p/abook/git"
EGIT_BOOTSTRAP="eautoreconf"

DESCRIPTION="Abook is a text-based addressbook program designed to use with mutt mail client."
HOMEPAGE="http://abook.sourceforge.net/"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ppc ~ppc64 ~sparc ~x86 ~x86-interix ~amd64-linux ~x86-linux ~ppc-macos ~x86-macos ~sparc-solaris ~x86-solaris"
IUSE="nls vcf"

DEPEND="nls? ( sys-devel/gettext ) vcf? ( dev-libs/libvformat )"
RDEPEND="${DEPEND}
		 sys-libs/ncurses
		 sys-libs/readline
		 nls? ( virtual/libintl )"

src_configure() {
	econf $(use_enable nls) $(use_enable vcf vformat)
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc BUGS ChangeLog FAQ README TODO sample.abookrc || die "dodoc failed"
}
