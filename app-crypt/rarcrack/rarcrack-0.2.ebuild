# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
inherit eutils

IUSE=""

DESCRIPTION="Program that bruteforces password of compressed archive (rar, 7z, zip)"
HOMEPAGE="http://rarcrack.sourceforge.net"
KEYWORDS="~amd64 ~x86"
SRC_URI="mirror://sourceforge/rarcrack/${P}.tar.bz2"

RDEPEND="dev-libs/libxml2
		 dev-libs/libpthread-stubs
		 app-arch/p7zip
		 app-arch/unzip
		 || ( app-arch/unrar app-arch/unrar-gpl )"
DEPEND="${RDEPEND}"

LICENSE="GPL-2"
SLOT="0"

src_prepare () {
	epatch "${FILESDIR}/wrong_type_segfault.patch"
	epatch "${FILESDIR}/mime_type.patch"
}

#src_test() {
#	./rarcrack test.rar || die "running test failed"
#}

src_install () {
	dobin rarcrack
	dodoc CHANGELOG README README.html RELEASE_NOTES
}
