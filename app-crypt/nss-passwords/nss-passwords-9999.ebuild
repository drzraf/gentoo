# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"

inherit eutils autotools git-2

EGIT_REPO_URI="git://github.com/glondu/nss-passwords.git"

DESCRIPTION="Reads passwords from a Mozilla keyring"
HOMEPAGE="https://github.com/glondu/nss-passwords"
KEYWORDS="~amd64 ~x86"

IUSE=""

RDEPEND="dev-ml/ocaml-fileutils
		 dev-ml/ocaml-sqlite3"
DEPEND="${RDEPEND}"

LICENSE="GPL-2"
SLOT="0"

src_install () {
	:
}
