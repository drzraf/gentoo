# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3


PYTHON_DEPEND="2"
SUPPORT_PYTHON_ABIS="1"
RESTRICT_PYTHON_ABIS="3.*"

inherit distutils

DESCRIPTION="A Python SOCKS module"
HOMEPAGE="http://socksipy.sourceforge.net/"
SRC_URI="mirror://sourceforge/socksipy/${PN}.tar.gz"

LICENSE="BSD-2"
SLOT="0"
#KEYWORDS="~amd64 ~x86"
IUSE=""

#PYTHON_MODNAME="SocksiPy"

DOCS="README LICENSE BUGS"
#DISTUTILS_SETUP_FILES=( socks.py )

distutils_src_install_pre_hook() {
	insinto "$(python_get_sitedir)/${PN}"
}

src_install() {
	distutils_src_install
	doins socks.py
}
