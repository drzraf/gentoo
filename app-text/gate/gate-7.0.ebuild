# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-text/gate/gate-5.0.ebuild,v 1.0 2009/03/19 11:23:02 droz_raph Exp $

DESCRIPTION="CMU Speech Recognition engine"
HOMEPAGE="http://gate.ac.uk"
MY_BUILD="4195"
SRC_URI="mirror://sourceforge/${PN}/${P}-build${MY_BUILD}-SRC.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="app-arch/unzip"

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog NEWS README
	dodoc -r doc
}
