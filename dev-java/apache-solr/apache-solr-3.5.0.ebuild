# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

JAVA_PKG_IUSE="doc example test tomcat"
JAVA_PKG_BSFIX_ALL="no"
JAVA_PKG_BSFIX_NAME="build.xml"

inherit java-pkg-2 java-ant-2 webapp

DESCRIPTION="An open-source search server based on the Lucene Java search library."
HOMEPAGE="http://lucene.apache.org/solr"
SRC_URI="mirror://apache/lucene/solr/${PV}/${P}-src.tgz"
LICENSE="Apache-2.0"

KEYWORDS="~x86 ~amd64"

SLOT="${PV}"

S="${WORKDIR}/${P}/solr"

CDEPEND="dev-java/lucene:3.0"

DEPEND="${CDEPEND}
	app-admin/webapp-config
	>=virtual/jdk-1.5
	dev-java/ant-core
	dev-java/javacc"

RDEPEND="${CDEPEND}
	>=virtual/jre-1.5"

pkg_setup() {
	webapp_pkg_setup
	java-pkg-2_pkg_setup
}

src_compile() {
	ANT_TASKS="none" eant compile dist
	use example && ANT_TASKS="none" eant example
	use doc     && ANT_TASKS="none" eant javadocs
}

src_test() {
	ANT_TASKS="none" eant test
}

src_install() {
	webapp_src_preinst
	exampledir=/var/www/localhost/htdocs/${PN}
	destdir=/usr/share/${P}

	solr_conf_src=${FILESDIR}/solrconfig.xml
	solr_conf_dest=${S}/example/solr/conf/solrconfig.xml

	sed s/EBUILD_SOLR_RELEASE/${P}/ ${solr_conf_src} > ${solr_conf_dest}

	use tomcat && {
		sed -ir "/dataDir/s?:?:${exampledir}?" ${solr_conf_dest}
		[[ ! -d /etc/tomcat-6/Catalina/localhost ]] && \
			dodir /etc/tomcat-6/Catalina/localhost

		sed -e "s;WARHOMEDIR;${destdir};" \
			-e "s;EXAMPLEDIR;${exampledir};" \
			${FILESDIR}/solr-example.xml > solr-example.xml
		insinto /etc/tomcat-6/Catalina/localhost/
		doins solr-example.xml
		webapp_configfile /etc/tomcat-6/Catalina/localhost/solr-example.xml
	}

	insinto "${MY_HTDOCSDIR}"
	doins -r example/solr/*
	webapp_configfile "${MY_HTDOCSDIR}"/conf/solrconfig.xml

	dodir ${MY_HTDOCSDIR}/bin
	dodir ${MY_HTDOCSDIR}/data

	webapp_serverowned -R "${MY_HTDOCSDIR}"

	insinto "${destdir}"
	newins dist/apache-solr-${PV%.*}-SNAPSHOT.war solr.war

	for contrib_dir in analysis-extras clustering dataimporthandler extraction langid uima velocity; do
		java-pkg_jarinto ${destdir}/contrib/${contrib_dir}/lib
		for contrib_lib in contrib/${contrib_dir}/lib/*.jar; do
			contrib_target=$(basename ${contrib_lib}| sed 's/\(^.*\)-[0-9][0-9\._][0-9\._].*\.jar$/\1.jar/' )
			java-pkg_newjar ${contrib_lib} ${contrib_target}
		done
	done

	java-pkg_jarinto /usr/share/${P}/dist

	for dist_lib in dist/*.jar
	do
		dist_target=$(basename ${dist_lib}|sed 's/-[0-9\.]*-SNAPSHOT//' )
		java-pkg_newjar ${dist_lib} ${dist_target}

	done

	dodoc -r site
	use doc && java-pkg_dojavadoc build/docs/api
	webapp_src_install
}

pkg_postinst() {
	webapp_pkg_postinst
	#use tomcat && chown -R tomcat.tomcat ${exampledir}/*
}
