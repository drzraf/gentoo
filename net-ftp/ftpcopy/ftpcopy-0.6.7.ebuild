# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# from http://portage.org.ua/portage/net-ftp/ftpcopy/ftpcopy-0.6.7.ebuild

SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~alpha amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~ppc-macos ~s390 ~sh ~sparc x86 ~x86-fbsd"
DESCRIPTION="A simple script to ftpcopy packages"
SRC_URI="http://www.ohse.de/uwe/ftpcopy/${P}.tar.gz"
HOMEPAGE="http://www.ohse.de/uwe/ftpcopy.html"
IUSE=""

S=${WORKDIR}/web/${P}

src_configure() {
	:
}

src_install() {
	emake DESTDIR=${D} install
}
