# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-accessibility/sphinx4/sphinx4-1.0_beta5.ebuild,v 0.1 2009/03/17 11:13:02 drzraf Exp $

inherit eutils java-pkg-2 java-ant-2

DESCRIPTION="CMU Speech Recognition engine"
HOMEPAGE="http://fife.speech.cs.cmu.edu/sphinx/"
SRC_URI="mirror://sourceforge/cmusphinx/${P//_/}-src.zip"

DEPEND=">=virtual/jre-1.5
	jsapi? ( app-arch/sharutils )
	app-arch/unzip"

LICENSE="jsapi? ( sun-bcla-jsapi ) BSD as-is"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc jsapi"

src_unpack() {
		unpack ${A}
		cd "${S}/lib"

		chmod 0755 jsapi.sh
		epatch "${FILESDIR}/jsapi-gentoo.diff"
}

src_compile() {
		if use jsapi; then
				cd "${S}/lib"
				./jsapi.sh || die "jsapi.sh failed"
		fi
		cd "${S}"
	eant all
}
src_install() {
	JAVA_ANT_JAVADOC_INPUT_DIRS=.
	emake DESTDIR="${D}" install || die
	dodoc ANNOUNCE.txt README RELEASE_NOTES

	#doins demo
	#doins lib
	#doins include
		if use doc; then
				insinto /usr/share/doc/${PF}
				doins -r "${S}"/doc/* "${S}"/doc-files/*
		fi
}
