# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-wireless/ifscheme/ifscheme-1.7.ebuild,v 0.1 2011/06/11 23:23:02 droz_raph Exp $

EAPI=2
inherit eutils

PATCH_LEVEL=3

DESCRIPTION="scheme control for network interfaces"
HOMEPAGE="http://packages.qa.debian.org/i/ifscheme.html"
SRC_URI="mirror://debian/pool/main/i/${PN}/${PN}_${PV}.orig.tar.gz
		 mirror://debian/pool/main/i/${PN}/${PN}_${PV}-${PATCH_LEVEL}.debian.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

S=${WORKDIR}/${PN}.${PV}

src_prepare() {
	EPATCH_SOURCE="${WORKDIR}"/debian/patches	\
		EPATCH_SUFFIX="diff"	\
		EPATCH_FORCE=yes		\
		epatch
	epatch "${FILESDIR}"/${PN}-ifupdown.patch
}

src_install() {
	dosbin essidscan ifscheme ifscheme-mapping wifichoice.sh
	# do we really need them ?
	#newinitd ifscheme.init ifscheme
	#newconfd ifscheme.default ifscheme
	dodoc CHANGELOG README*
	doman essidscan.8 ifscheme.8
}


