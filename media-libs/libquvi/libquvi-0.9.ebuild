# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-libs/libquvi/libquvi-0.4.1.ebuild,v 1.15 2013/02/24 18:49:05 ago Exp $

EAPI=5

inherit autotools-utils git-2

DESCRIPTION="Library for parsing video download links"
HOMEPAGE="http://quvi.sourceforge.net/"
EGIT_REPO_URI="git://repo.or.cz/libquvi.git"
EGIT_BRANCH="next"

LICENSE="AGPL-3"
SLOT="1"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="examples static-libs"

RDEPEND="net-misc/curl
		 dev-lang/lua
		 media-libs/libquvi-scripts:1
		 net-libs/libproxy
		 dev-libs/glib:2
		 dev-libs/libgcrypt"

DEPEND="${RDEPEND}
	virtual/pkgconfig"

DOCS=( AUTHORS NEWS README )

src_prepare() {
	./bootstrap.sh
}

# --without-manual: slot => override
src_configure() {
	local myeconfargs=(
		--without-manual
	)
	autotools-utils_src_configure
}

src_install() {
	autotools-utils_src_install

	if use examples ; then
		docinto examples
		dodoc examples/*.{c,h}
	fi
}
