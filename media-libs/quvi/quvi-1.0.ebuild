# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2

DESCRIPTION="A command line tool for parsing video download links"
HOMEPAGE="http://quvi.sourceforge.net/"
EGIT_REPO_URI="git://repo.or.cz/quvi-tool.git"
EGIT_BRANCH="next"

LICENSE="AGPL-3"
SLOT="1"
KEYWORDS="~amd64 ppc ppc64 ~x86"
IUSE=""

# optional, but no configure flag available
# dev-libs/json-glib
# dev-libs/libxml
RDEPEND="net-misc/curl
	dev-libs/glib:2
	media-libs/libquvi:1"
DEPEND="${RDEPEND}
	app-arch/xz-utils
	virtual/pkgconfig"

src_configure() {
	./bootstrap.sh
	# --with-manual [default=yes]
	# but without manual to allow slot'ing
	econf --without-manual
}

src_install() {
	newbin src/quvi quvi-${PV} || die "newbin failed"
}
