# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit git-2

DESCRIPTION="Embedded lua scripts for libquvi"
HOMEPAGE="http://quvi.sourceforge.net/"
EGIT_REPO_URI="git://repo.or.cz/libquvi-scripts.git"
EGIT_BRANCH="next"

LICENSE="AGPL-3"
SLOT="1"
KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~ppc ~ppc64 ~sparc ~x86"
IUSE="offensive test"

RDEPEND="dev-lua/luaexpat
		 dev-lua/luasocket
		 dev-lua/LuaBitOp
		 dev-lua/luajson"

DEPEND="test? ( media-libs/libquvi:1 dev-libs/glib:2 net-misc/curl )"

# tests fetch data from live websites, so it's rather normal that they
# will fail
RESTRICT="test"

src_prepare() {
	./bootstrap.sh
}

# --without-manual: slot packages => collision
src_configure() {
	econf --without-manual $(use_with offensive nsfw) --with-fixme
}
