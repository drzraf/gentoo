# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit autotools eutils git-2

EGIT_REPO_URI="git://gitorious.org/nss-dothosts/nss-hostslocal.git"

DESCRIPTION="Name Service Switch module providing per-user .hosts file"
HOMEPAGE="http://gitorious.org/nss-dothosts/nss-hostslocal"
SRC_URI=""

LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE=""
DEPEND=""

src_install() {
	make DESTDIR="${D}" install
	dodoc README
}

pkg_postinst() {
	ewarn
	ewarn "You must modify your name service switch look up file to enable"
	ewarn "multicast DNS lookups."
	ewarn
	ewarn "Add the appropriate nss-dothosts into the hosts line in /etc/nsswitch.conf"
	ewarn "An example line looks like:"
	ewarn "hosts:	files hostslocal dns"
	ewarn
}
