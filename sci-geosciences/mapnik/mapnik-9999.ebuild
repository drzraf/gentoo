# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit eutils autotools git-2 scons-utils toolchain-funcs

EGIT_REPO_URI="git://github.com/mapnik/mapnik.git"

DESCRIPTION="A Free Toolkit for developing mapping applications."
HOMEPAGE="http://mapnik.org"
SRC_URI=""
LICENSE="LGPL-3"
SLOT="0"

KEYWORDS=""
IUSE="bidi cairo debug doc gdal geos nobfonts postgres python proj sqlite"

DEPEND="net-misc/curl
		media-libs/libpng
		virtual/jpeg
		media-libs/tiff
		sys-libs/zlib
		media-libs/freetype
		dev-lang/python
		proj? ( sci-libs/proj )
		dev-libs/libxml2
		dev-libs/icu
		x11-libs/agg[truetype]
		>=dev-libs/boost-1.41[python?]
		postgres? ( dev-db/postgis
					sci-libs/geos )
		gdal? ( sci-libs/gdal )
		python? ( dev-lang/python )
		bidi? ( dev-libs/fribidi )
		cairo? ( x11-libs/cairo dev-cpp/cairomm )
		sqlite? ( dev-db/sqlite )
		nobfonts? ( media-fonts/dejavu )"

PYTHON_DEPEND="python? 2"
PYTHON_CFLAGS=("2.*")

pkg_setup() {
	if use python; then
		:
		#python_set_active_version 2
		#python_pkg_setup
	fi
}

src_prepare() {
	epatch "${FILESDIR}"/${P}-gentoo.patch
}

src_configure() {
	local PLUGINS=shape,raster,osm
	use geos && PLUGINS+=,geos
	use postgres && PLUGINS+=,postgis
	use sqlite && PLUGINS+=,sqlite

	#$(use_scons proj PROJ_INCLUDES /usr/include '')
	#$(use_scons proj PROJ_LIBS=/usr/lib '')

	SCONOPTS="
		CC=$(tc-getCC)
		CXX=$(tc-getCXX)
		INPUT_PLUGINS=${PLUGINS}
		PREFIX=/usr
		XMLPARSER=libxml2
		PROJ_INCLUDES=/usr/include
		PROJ_LIBS=/usr/lib
		$(use_scons nobfonts SYSTEM_FONTS /usr/share/fonts '')
		$(use_scons python BINDINGS all none)
		$(use_scons python BOOST_PYTHON_LIB boost_python-${PYTHON_ABI})
		$(use_scons bidi BIDI)
		$(use_scons cairo CAIRO)
		$(use_scons debug DEBUG)
		$(use_scons debug XML_DEBUG)
		$(use_scons doc DEMO)
		$(use_scons doc SAMPLE_INPUT_PLUGINS)
		CUSTOM_LDFLAGS=${LDFLAGS}
		CUSTOM_LDFLAGS+=-L${D}/usr/$(get_libdir)"

	# force user flags, optimization level
	sed -i -e "s:\-O%s:${CXXFLAGS}:" \
		-i -e "s:env\['OPTIMIZATION'\]\,::" \
		SConstruct || die "sed 3 failed"

	scons $SCONOPTS configure || die "scons configure failed"
}

src_compile() {
	scons ${MAKEOPTS} shared=1 || die "scons compile failed"
}

src_install() {
	#the lib itself still seems to need a DESTDIR definition
	scons DESTDIR="${D}" install || die "scons install failed"

	if use python ; then
		fperms 0644 "$(python_get_sitedir)"/mapnik/paths.py
		dobin utils/stats/mapdef_stats.py
	fi

	dodoc AUTHORS.md README.md

	use doc && {
		mkdir -p docs/epydocs
		epydoc --no-private --no-frames --no-sourcecode --name mapnik \
			--url http://mapnik.org --css utils/epydoc_config/mapnik_epydoc.css \
			mapnik -o docs/epydocs/
		dohtml -r docs/epydocs/*
	}
}
