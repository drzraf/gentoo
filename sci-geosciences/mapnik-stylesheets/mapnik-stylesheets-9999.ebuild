# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils git-2

EGIT_REPO_URI="git://github.com/openstreetmap/mapnik-stylesheets.git"

DESCRIPTION="The Mapnik XML stylesheets powering tile.openstreetmap.org"
HOMEPAGE="http://www.openstreetmap.org"
SRC_URI=""

#LICENSE="?"
SLOT="0"
KEYWORDS=""

IUSE=""

COMMON_DEP=""
