# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit eutils autotools subversion scons-utils

ESVN_REPO_URI="http://mapnik-utils.googlecode.com/svn/trunk/"
ESVN_PROJECT="${PN}"

DESCRIPTION="Python tools for working with Mapnik"
HOMEPAGE="http://code.google.com/p/mapnik-utils"
SRC_URI=""
LICENSE="GPL-2 BSD"
SLOT="0"

KEYWORDS=""
IUSE=""

DEPEND=""
